//
//  QRCodeReader.swift
//  VisionQRReader
//
//  Created by Tzu Yi Lin on 2018/1/1.
//  Copyright © 2018年 Kingwaytek. All rights reserved.
//

import UIKit
import AVFoundation
import Vision

class QRCodeReader: UIControl {

    private var captureSession = AVCaptureSession()

    private var previewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }

    private lazy var barcodeRequest: VNDetectBarcodesRequest = {
        let request = VNDetectBarcodesRequest(completionHandler: self.barcodeCompletionHandler)
        request.symbologies = [.QR]
        return request
    }()

    private let operationQueue = DispatchQueue(label: "qrcode reader queue")

    private(set) var codes: [String] = [] {
        didSet {
            sendActions(for: .valueChanged)
        }
    }

    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configCaptureSession()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configCaptureSession()
    }

    override func didMoveToWindow() {
        super.didMoveToWindow()
        if window != nil {
            captureSession.startRunning()
        } else {
            captureSession.stopRunning()
        }
    }

    private func configCaptureSession() {

        previewLayer.session = captureSession
        previewLayer.videoGravity = .resizeAspectFill

        if let camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) {

            do {
                let input = try AVCaptureDeviceInput(device: camera)
                captureSession.addInput(input)

                let output = AVCaptureVideoDataOutput()
                output.setSampleBufferDelegate(self, queue: operationQueue)
                captureSession.addOutput(output)

            } catch {
                print(error)
            }
        }
    }

    private func barcodeCompletionHandler(request: VNRequest, error: Error?) {

        if let result = request.results, let observations = result as? [VNBarcodeObservation], !observations.isEmpty {

            DispatchQueue.main.async {

                self.codes = observations.flatMap { $0.payloadStringValue }
                let transform = CGAffineTransform(translationX: 0, y: 1).scaledBy(x: 1, y: -1)

                let borderLayers = observations.map { obs -> CAShapeLayer in
                    let tl = self.previewLayer.layerPointConverted(fromCaptureDevicePoint: obs.topLeft.applying(transform))
                    let tr = self.previewLayer.layerPointConverted(fromCaptureDevicePoint: obs.topRight.applying(transform))
                    let bl = self.previewLayer.layerPointConverted(fromCaptureDevicePoint: obs.bottomLeft.applying(transform))
                    let br = self.previewLayer.layerPointConverted(fromCaptureDevicePoint: obs.bottomRight.applying(transform))

                    let layer = CAShapeLayer()
                    let path = CGMutablePath()

                    path.move(to: tl)
                    path.addLine(to: tr)
                    path.addLine(to: br)
                    path.addLine(to: bl)
                    path.addLine(to: tl)

                    layer.name = "borderLayer"
                    layer.fillColor = UIColor.clear.cgColor
                    layer.strokeColor = UIColor.green.cgColor
                    layer.lineWidth = 2.0
                    layer.path = path
                    return layer
                }


                self.layer.sublayers?
                    .filter { $0.name == "borderLayer" }
                    .forEach { $0.removeFromSuperlayer() }
                borderLayers.forEach { self.layer.addSublayer($0) }
            }
        }
    }
}

extension QRCodeReader: AVCaptureVideoDataOutputSampleBufferDelegate {

    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }

        var requestOptions: [VNImageOption : Any] = [:]

        if let data = CMGetAttachment(sampleBuffer, kCMSampleBufferAttachmentKey_CameraIntrinsicMatrix, nil) {
            requestOptions[.cameraIntrinsics] = data;
        }

        let handler = VNImageRequestHandler(cvPixelBuffer: imageBuffer, options: requestOptions)

        do {
            try handler.perform([barcodeRequest])
        } catch {
            print(error)
        }
    }
}
